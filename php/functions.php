<?php

function rootDirectory() {
    $dir = 'content'; // root directory
    $filesToSkip = array('.', '..');
    $content = array_diff(scandir($dir), $filesToSkip);
    $folderList = array();
    foreach ($content as $file) {
        $filePath = $dir . '/' . $file;
        if (is_dir($filePath)) {

            // array for each folder
            $sFolder = array(
                'name' => $file,
                'filePath' => $filePath
            );

            array_push($folderList, $sFolder);
        }
    }
    return $folderList;
}

function listContent() {
    $dir = 'content'; // root directory
    if (isset($_GET['filepath'])) {
        $currentFP = $_GET['filepath'];
        // prevents user from using .. in url
        $currentFP = str_replace('/..', '', $currentFP);
        $dir = $currentFP;
    } elseif (isset($_GET['download'])) {
        $currentFP = $_GET['download'];
        $currentFP = str_replace('/..', '', $currentFP);
        $dir = $currentFP;
        download();
    }
    $filesToSkip = array('.', '..');
    // exclude . & .. from content list
    $content = array_diff(scandir($dir), $filesToSkip);

    $fileList = array();
    foreach ($content as $file) { // for each file in the folder
        $filePath = $dir . '/' . $file;
        if (is_dir($filePath)) {
            $mDate = '';
            $fileType = 'folder';
        } else {
            $mDate = date('d/m/Y', filemtime($filePath)); // modif date
            $lastDot = strrpos($filePath, '.'); // Last dot
            $fileType = substr($filePath, $lastDot); // isolate file type
        }

        // array for each file/folder
        $sFile = array(
            'name' => $file,
            'filePath' => $filePath,
            'fileType' => $fileType,
            'mDate' => $mDate
        );

        array_push($fileList, $sFile);
    }

    return $fileList;
}

function parentFolder() {
    $dir = 'content'; // root directory
    if (isset($_GET['filepath'])) {
        $currentFP = $_GET['filepath'];
        // prevents user from using .. in url
        $currentFP = str_replace('/..', '', $currentFP);
        $dir = $currentFP;
    }

    $parentFolder = 'content';
    if ($dir !== 'content') { // if not in root directory
        $lastSlash = strrpos($dir, '/'); // position of last slash
        // substract last folder from filepath
        $parentFolder = substr($dir, 0, - strlen($dir) + $lastSlash);
    }

    return $parentFolder; // filepath without last dir
}

function directory() {
    if (isset($_GET['filepath'])) {
        $dir = $_GET['filepath'];
        $filePath = str_replace("content", "", $dir);
        $filePath = ltrim($filePath, "/");
        $filePath = str_replace("/", " > ", $filePath);
        return $filePath;
    }
}

function download() {
    if(isset($_GET['download'])){
        $file = $_GET['download'];
        if(is_dir($file) === false) {
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.
                    basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                readfile($file);
                ob_flush();
                exit;
            }
        }
    }
}

?>
