
<?php
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include 'php/functions.php';

echo $twig->render("index.twig",
    ["parentFolder" => parentFolder(),
    "directory" => directory(),
    "rootDirectory" => rootDirectory(),
    "content" => listContent()]
);

?>
